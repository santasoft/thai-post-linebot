import * as functions from 'firebase-functions';
import * as requestPromise from 'request-promise';
import { FlexBox, FlexMessage } from '@line/bot-sdk';

const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message';
// const LINE_UID = ${functions.config().line.uid};
const LINE_HEADER = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${functions.config().line.header_token}`
};
const TH_POSTAL_API_TOKEN = functions.config().th_postal.api_token;
const TH_POSTAL_URI = 'https://trackapi.thailandpost.co.th/post/api/v1';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
export const helloWorld = functions.region('asia-east2').https.onRequest((request, response) => {
    if (request.body.events[0].message.type == 'text') {
        reply(request.body);
    }
    response.status(200).send(request.method);
});

const reply = async (bodyResponse: { events: any[]; }) => {
    const trackingNo = bodyResponse.events[0].message.text.trim().replace(/\s/g, '');
    console.log(`Tracking No. => ${trackingNo}`);
    var postalRequestToken: any = await fetchPostalToken;

    var tracks: any = await trackPostal(postalRequestToken.token, [trackingNo]);
    if (tracks === null || tracks.response.items[trackingNo].length <= 0) {
        replyNotFound(bodyResponse, trackingNo);
    } else {
        replyTrack(bodyResponse, tracks);
    }
};

const trackPostal = async (accessToken: string, trackingNos: string[]) => {
    const params = {
        'status': 'all',
        'language': 'TH',
        'barcode': trackingNos
    };
    const trackingPromise = new Promise(resolve => {
        var options = {
            method: 'POST',
            uri: `${TH_POSTAL_URI}/track`,
            strictSSL: false,
            body: JSON.stringify(params),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${accessToken}`
            }
        };
        requestPromise(options, (error: any, response: any, body: any) => {
            resolve(JSON.parse(body));
        });
    });

    const tracks = await trackingPromise;
    return tracks;
}

const fetchPostalToken = new Promise(resolve => {
    var options = {
        method: 'POST',
        uri: `${TH_POSTAL_URI}/authenticate/token`,
        strictSSL: false,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${TH_POSTAL_API_TOKEN}`
        }
    };

    requestPromise(options, (error, response, body) => {
        resolve(JSON.parse(body))
    })
});

const replyNotFound = (requestBody: any, trackingNo: string) => {
    return requestPromise({
        method: 'POST',
        uri: `${LINE_MESSAGING_API}/reply`,
        headers: LINE_HEADER,
        body: JSON.stringify({
            replyToken: requestBody.events[0].replyToken,
            messages: [
                {
                    type: 'text',
                    text: `Tracking ${trackingNo} not found`
                }
            ]
        })
    });
}

const replyTrack = (requestBody: any, tracks: any) => {
    var trackNoList = Object.keys(tracks.response.items);
    var templateMsg: FlexMessage[] = [];
    trackNoList.forEach((trackNo: string) => {
        var trackList = tracks.response.items[trackNo];
        var tracksContent = __makeTrackListToMessage(trackList);
        var itemFlexMsg: FlexMessage = {
            type: 'flex',
            altText: 'Delivery Status',
            contents: {
                type: 'carousel',
                contents: [{
                    type: 'bubble',
                    body: {
                        type: 'box',
                        layout: 'vertical',
                        contents: [{
                            type: 'text',
                            text: trackNo,
                            size: 'lg',
                            weight: 'bold'
                        }, {
                            type: 'box',
                            layout: 'vertical',
                            contents: tracksContent.tracklistBox,
                            spacing: 'md',
                            margin: 'md'
                        }]
                    },
                    styles: {
                        body: {
                            backgroundColor: tracksContent.deliverStatusColor
                        }
                    }
                }]
            }
        };
        templateMsg.push(itemFlexMsg);
    });
    return requestPromise({
        method: 'POST',
        uri: `${LINE_MESSAGING_API}/reply`,
        headers: LINE_HEADER,
        body: JSON.stringify({
            replyToken: requestBody.events[0].replyToken,
            messages: templateMsg
        })
    });
}

function __makeTrackListToMessage(tracklist: any[]): any {
    var __tracklistBox: FlexBox[] = [];
    var bgColor = '#EEEEEE';
    tracklist.forEach((tracking: any) => {
        bgColor = tracking.delivery_status === 'S' ? '#ABEBC6' : '#EEEEEE';
        var trackBox: FlexBox = {
            type: 'box',
            layout: 'horizontal',
            contents: [{
                type: 'box',
                layout: 'vertical',
                contents: [{
                    type: 'box',
                    layout: 'horizontal',
                    contents: [{
                        type: 'text',
                        text: tracking.status_date
                    }]
                }, {
                    type: 'box',
                    layout: 'horizontal',
                    contents: [{
                        type: 'text',
                        text: `  ${tracking.status_description}`,
                        size: 'sm'
                    }]
                }, {
                    type: 'box',
                    layout: 'horizontal',
                    contents: [{
                        type: 'text',
                        text: `  ${tracking.location}`,
                        size: 'sm'
                    }, {
                        type: 'text',
                        text: tracking.postcode,
                        size: 'sm'
                    }]
                }]
            }]
        };

        __tracklistBox.push(trackBox);
    });
    var tracklistBoxData: { deliverStatusColor: string, tracklistBox: FlexBox[] } = {
        deliverStatusColor: bgColor,
        tracklistBox: __tracklistBox
    }
    return tracklistBoxData;
}
